const getSum = (str1, str2) => {
  if(str1 === '') {
    str1 = '0';
   } else if(str2 === '') {
    str2 = '0';
   }
  if(typeof str1 !== 'string' || typeof str2 !== 'string' || str1.match(/^\d+$/) === null || str2.match(/^\d+$/) === null) {
    return false;
  } else {
    return (BigInt(str1) + BigInt(str2)).toString();
  }
};
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comments = 0;
  listOfPosts.forEach(item => {
    if(item['author'] === authorName) {
      post++;
    }
    if(item.comments !== undefined) {
      item.comments.forEach(el => {
        let keys = Object.keys(el);
        keys.forEach(key => {
          if(el[key] === authorName) {
            comments++;
          }
        })
      })
    }
  })
  return 'Post:' + post + ',comments:' + comments;
};
const tickets=(people)=> {
  let b25 = 0, b50 = 0;
  let a = people.map(Number);
  for(let value of a){
      if(value === 25){
        ++b25;
      }
      if(value === 50){
          ++b50;
          b25--;
      }
      else if(value === 100){
          if(b50 >= 1 && b25 >= 1){
            b50--;
            --b25;
          } else {
            b25-=3;
          }
      }
      if(b25 < 0 || b50 < 0){
          return 'NO';
      }
  }
  return 'YES';  
};
module.exports = {getSum, getQuantityPostsByAuthor, tickets};
